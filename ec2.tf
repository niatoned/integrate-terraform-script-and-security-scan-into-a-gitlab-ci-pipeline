resource "aws_instance" "public_instance" {
  ami           = "ami-089c26792dcb1fbd4"
  instance_type = "t2.micro"
  key_name = "mykey"
  subnet_id = aws_subnet.public_subnet.id
associate_public_ip_address = true
  vpc_security_group_ids = [aws_security_group.public_sg.id]
  availability_zone = "us-east-2a"

  tags = {
    Name = "${var.project_name}-public-ec2"
  }
}

resource "aws_instance" "private_instance" {
  ami           = "ami-089c26792dcb1fbd4"
  instance_type = "t2.micro"
  key_name = "mykey"
  subnet_id = aws_subnet.private_subnet.id
  vpc_security_group_ids = [aws_security_group.private_sg.id]
  availability_zone = "us-east-2a"

  tags = {
    Name = "${var.project_name}-private-ec2"
  }
}