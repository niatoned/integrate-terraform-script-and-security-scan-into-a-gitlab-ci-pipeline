terraform {
    backend "s3" {
      bucket = "dnd-bucket-test1"
      key = "tfstate/terraform.tfstate"
      region = "us-east-2"
    }
}